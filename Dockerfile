FROM ubuntu:14.04

# Note for 14.04 is software-properties-common and for 12.04 python-software-properties
RUN apt-get update && apt-get install -y software-properties-common curl && add-apt-repository ppa:webupd8team/java && echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && apt-get update 

RUN apt-get install -y oracle-java7-installer supervisor nginx && curl -L https://download.elasticsearch.org/logstash/logstash/packages/debian/logstash_1.4.2-1-2c0f5a1_all.deb --output /opt/logstash-1-4-2.deb && dpkg -i /opt/logstash-1-4-2.deb && curl -L https://download.elasticsearch.org/logstash/logstash/packages/debian/logstash-contrib_1.4.2-1-efd53ef_all.deb --output /opt/logstash-contrib-1-4-2.deb && dpkg -i /opt/logstash-contrib-1-4-2.deb && curl -L https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.3.4.deb  --output /opt/elasticsearch-1-3-4.deb && dpkg -i /opt/elasticsearch-1-3-4.deb && apt-get -y autoclean 

ADD elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml
ADD logstash/ /etc/logstash/conf.d/
ADD supervisord/program.conf /etc/supervisor/conf.d/program.conf

EXPOSE 5000 9200

VOLUME /etc/logstash/conf.d/

CMD /usr/bin/supervisord
